// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SphereComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ColissionActor.generated.h"

UCLASS()
class CS378_LAB3_API AColissionActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AColissionActor();
    
    FORCEINLINE class UStaticMeshComponent *  GetMeshComponent() const {return MeshComponent;}
    FORCEINLINE class USphereComponent * GetTriggerComponent() const {return TriggerComponent;}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent *MeshComponent;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    USphereComponent *TriggerComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
