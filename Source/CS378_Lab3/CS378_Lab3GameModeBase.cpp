// Copyright Epic Games, Inc. All Rights Reserved.


#include "WorldPawn.h"
#include "CS378_Lab3GameModeBase.h"

ACS378_Lab3GameModeBase::ACS378_Lab3GameModeBase()
{
    // set default pawn class to our character class
//    DefaultPawnClass = ACS378_Lab1Pawn::StaticClass();
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/MyWorldPawnBP.MyWorldPawnBP_C'"));
     if (pawnBPClass.Object)
    {
        if (GEngine)
        {
         GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
        }
        UClass* pawnBP = (UClass* )pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
        DefaultPawnClass = AWorldPawn::StaticClass();
    }
    
}


                                                             

