// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Actor.h"



#include "ResponseActor.h"

// Sets default values
AResponseActor::AResponseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    //Components
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
    setPointer = false;

}

// Called when the game starts or when spawned
void AResponseActor::BeginPlay()
{
	Super::BeginPlay();
    if (triggerActorBound and !setPointer)
    {
        triggerActorBound->OnTriggerDelegate.AddDynamic(this, &AResponseActor::RecieveSignal);
        setPointer = true;
        
        
    }
    
}

// Called every frame
void AResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (triggerActorBound and !setPointer)
    {
        triggerActorBound->OnTriggerDelegate.AddDynamic(this, &AResponseActor::RecieveSignal);
        setPointer = true;
        
        
    }

}

void AResponseActor::RecieveSignal()
{
    if (GEngine)
    {
     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("ResponseActor has been signaled"));
    }
    FVector loc = this->GetActorLocation();
    FVector pos = FVector(10.0f, 10.0f, 10.0f) + loc;
    this->SetActorLocation(pos);
}

