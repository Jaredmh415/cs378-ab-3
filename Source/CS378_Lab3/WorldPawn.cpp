// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "ColissionActor.h"
#include "ResponseActor.h"
#include "TriggerActor.h"

#include "Components/BoxComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "WorldPawn.h"

const FName AWorldPawn::MoveForwardBinding("MoveForwardBinding");
const FName AWorldPawn::MoveRightBinding("MoveRightBinding");
const FName AWorldPawn::InteractBinding("InteractBinding");

// Sets default values
AWorldPawn::AWorldPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    
    
    //Components
    
    /*
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
     */
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    RootComponent = MeshComponent;
    
    
    HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
    HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
    // Movement
    MoveSpeed = 1000.0f;
    
    
    
    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
    CameraBoom->TargetArmLength = 1200.f;
    CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false;    // Camera does not rotate relative to arm

}

// Called when the game starts or when spawned
void AWorldPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    
    
    // Find movement direction
    const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
    const float RightValue = GetInputAxisValue(MoveRightBinding);

    // Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
    const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);
    
    // Calculate  movement
    const FVector Movement = MoveDirection * MoveSpeed * DeltaTime;
    

    // If non-zero size, move this actor
    if (Movement.SizeSquared() > 0.0f)
    {
        const FRotator NewRotation = Movement.Rotation();
        FHitResult Hit(1.f);
        RootComponent->MoveComponent(Movement, NewRotation, true, &Hit);
        
        if (Hit.IsValidBlockingHit())
        {
            const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
            const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
            RootComponent->MoveComponent(Deflection, NewRotation, true);
        }
    }
    
    

}

// Called to bind functionality to input
void AWorldPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);
    check(PlayerInputComponent);

   // set up gameplay key bindings
   PlayerInputComponent->BindAxis(MoveForwardBinding);
   PlayerInputComponent->BindAxis(MoveRightBinding);
//
   PlayerInputComponent->BindAction(InteractBinding, IE_Pressed, this, &AWorldPawn::InteractPressed);

}

void AWorldPawn::PerformInteraction()
{
    if (GEngine)
    {
        FString DebugMsg = FString::Printf(TEXT("Interact Pressed"));
        GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, DebugMsg);
    }
    TArray<AActor *> OverlappingActors;
    HitboxComponent->GetOverlappingActors(OverlappingActors);
    for (AActor * actor: OverlappingActors)
    {
        if(actor->IsA(AColissionActor::StaticClass()))
        {
            if (GEngine)
            {
                FString DebugMsg = FString::Printf(TEXT("CollisionActor destroyed"));
                GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, DebugMsg);
            }
            actor->Destroy();
            
        }
        if(actor->IsA(ATriggerActor::StaticClass()))
        {
            if (GEngine)
            {
                FString DebugMsg = FString::Printf(TEXT("TriggerActor activated"));
                GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, DebugMsg);
            }
            ATriggerActor * actorSignal = Cast<ATriggerActor>(actor);
            actorSignal->OnTriggerDelegate.Broadcast();
            
        }
    }
    
}

